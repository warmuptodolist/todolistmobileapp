/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { ApolloProvider } from 'react-apollo';
import TodoApp from './src/screens/Home';
import { apolloClient } from './src/config';
import { Loading } from './src/components';
import { store } from './src/redux/store';
import { Provider } from 'react-redux';
import ReduxApp from './src/redux/action';
import { Root } from 'native-base';

export default class App extends Component {
	constructor() {
		super();
		this.loading;
	}
	render() {
		return (
			<Provider store={store}>
				<ApolloProvider client={apolloClient}>
					<Root>
						<TodoApp />
						<Loading ref={(ref) => store.dispatch(ReduxApp.addLoading(ref))} />
					</Root>
				</ApolloProvider>
			</Provider>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF'
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5
	}
});
