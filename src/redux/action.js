import { APP } from '../config';

class ReduxApp {
	addLoading(payload) {
		return {
			type: APP.ADD_LOADING,
			payload: payload
		};
	}
}

export default new ReduxApp();
