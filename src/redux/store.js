import reducer from './reducer';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';

let store = createStore(reducer, applyMiddleware(logger));

export { store };
