import { APP } from '../config';
import { combineReducers } from 'redux';
let initialState = {
	loading: {}
};

const appReducer = (state = initialState, action) => {
	switch (action.type) {
		case APP.ADD_LOADING:
			return {
				...state,
				loading: action.payload
			};

		default:
			return {
				...state
			};
	}
};

let reducer = combineReducers({
	appReducer
});

export default reducer;
