import { ApolloClient, createNetworkInterface } from "apollo-client";
import { SERVER_URL } from "./Constants";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";

const link = new HttpLink({ uri: SERVER_URL });
const cache = new InMemoryCache();
const apolloClient = new ApolloClient({
  link,
  cache
});

export { apolloClient };
export default apolloClient;
