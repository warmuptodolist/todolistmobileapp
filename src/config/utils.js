import { Toast } from 'native-base';

export const loadingTimeout = (cb = () => {}) => {
	setTimeout(cb, 3000);
};

export const showToast = (message = '', isError = false) => {
	Toast.show({
		text: message,
		type: isError ? 'danger' : 'success'
	});
};
