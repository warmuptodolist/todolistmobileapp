import React, { Component } from "react";
import { View } from "react-native";
import { Item, Input, Button, Text } from "native-base";
class CusInput extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  static defaultProps = {
    onPress: () => {},
    onChangeText: e => {},
    value: ""
  };
  render() {
    let { onPress, value, onChangeText } = this.props;
    return (
      <View>
        <View style={{ marginBottom: 20, flexDirection: "row" }}>
          <Item regular style={{ width: "80%" }}>
            <Input
              placeholder="Add Todo"
              value={value}
              onChangeText={onChangeText}
            />
          </Item>
          <Button
            full
            style={{ height: "100%", width: "20%" }}
            onPress={onPress}
          >
            <Text>Save</Text>
          </Button>
        </View>
      </View>
    );
  }
}

export { CusInput };
