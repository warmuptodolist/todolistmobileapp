import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { loadingTimeout } from '../../config';

class Loading extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoadingPlay: false
		};
	}

	static defaultProps = {
		loadingText: 'Loading...'
	};

	play = () => {
		console.log('call loading =====>');
		this.setState({
			isLoadingPlay: true
		});
	};

	stop = () => {
		this.setState({
			isLoadingPlay: false
		});
	};

	render() {
		let { isLoadingPlay } = this.state;
		let { loadingText } = this.props;
		return <Spinner visible={isLoadingPlay} textContent={loadingText} textStyle={{ color: 'white' }} />;
	}
}

export { Loading };
