import gql from "graphql-tag";

const QUERY = gql`
  query {
    todos {
      id
      todo
      isSelect
    }
  }
`;

export { QUERY };
export default QUERY;
