import React, { Component } from 'react';
import { View, Text, ListView } from 'react-native';
import { Container, Header, Content, Button, Icon, List, ListItem, Item, Input } from 'native-base';
export default class TodoList extends Component {
	constructor(props) {
		super(props);
		this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
		this.state = {
			basic: true,
			listViewData: []
		};
	}

	static defaultProps = {
		todos: [],
		numberToOpen: 100,
		disableLeftSwipe: false
	};

	render() {
		let { todos, onDelete, onEdit, numberToOpen, disableLeftSwipe } = this.props;

		return (
			<Content>
				<List
					leftOpenValue={numberToOpen}
					rightOpenValue={numberToOpen - numberToOpen * 2}
					dataSource={this.ds.cloneWithRows(todos)}
					renderRow={(data) => (
						<ListItem>
							<Text numberOfLines={1}> {data.todo} </Text>
						</ListItem>
					)}
					renderRightHiddenRow={(data, secId, rowId, rowMap) => (
						<View style={{ flexDirection: 'row' }}>
							<Button
								full
								primary
								onPress={() => {
									rowMap[`${secId}${rowId}`].props.closeRow();
									onEdit(data.id);
								}}
							>
								<Icon active name="md-create" />
							</Button>
							<Button
								full
								danger
								onPress={() => {
									onDelete(data.id, secId, rowId, rowMap);
								}}
							>
								<Icon active name="trash" />
							</Button>
						</View>
					)}
					disableRightSwipe
					closeOnRowBeginSwipe={true}
					disableLeftSwipe={disableLeftSwipe}
				/>
			</Content>
		);
	}
}
