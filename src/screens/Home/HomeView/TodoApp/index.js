import React, { Component } from 'react';
import { View, ListView, ActivityIndicator } from 'react-native';
import { Container, Header, Content, Button, Icon, List, ListItem, Text, Item, Input } from 'native-base';
import { CusInput } from '../../../../components';
import TodoList from '../TodoList/index';

import _ from 'lodash';
import Dialog from 'react-native-dialog';
import { connect } from 'react-redux';
import { showToast } from '../../../../config';

class TodoApp extends Component {
	constructor(props) {
		super(props);
		this.state = {
			todo: '',
			spinner: false,
			isDialogVisible: false,
			editedTodo: '',
			idToEdit: '',
			disableLeftSwipe: false
		};
	}

	handleCreateTodo = async () => {
		let { CreateTodo, loading } = this.props;
		let { todo } = this.state;
		if (todo != '') {
			try {
				loading.play();
				let response = await CreateTodo({ variables: { todo } });
				this.setState({
					todo: ''
				});
				loading.stop();
			} catch (e) {
				loading.stop();
				console.log('error', e);
				showToast(_.get(e, 'message', 'Something went wrong'), true);
			}
		} else {
			showToast('Please input text before you save', true);
		}
	};

	handleEdit = (id) => {
		this.setState({
			isDialogVisible: true,
			idToEdit: id
		});
	};

	handleCloseDialog = () => {
		this.setState({ isDialogVisible: false });
	};

	handleChangeEditedTodo = (text = '') => {
		this.setState({
			editedTodo: text
		});
	};

	handleEditTodo = async () => {
		let { editedTodo, idToEdit, numberToOpenRow } = this.state;
		let { UpdateTodo, QueryTodo, loading } = this.props;
		if (editedTodo != '') {
			try {
				this.setState({
					isDialogVisible: false,
					editedTodo: '',
					idToEdit: ''
				});
				let response = await UpdateTodo({ variables: { id: idToEdit, todo: editedTodo } });
				showToast('Edit successful');
			} catch (e) {
				let error = JSON.stringify(e);
				showToast(_.get(e, 'message', 'Something went wrong'), true);
			}
		} else {
			showToast('Please input text before you edit', true);
		}
	};

	handleDeleteTodo = async (id, secId, rowId, rowMap) => {
		let { DeleteTodo, QueryTodo, loading } = this.props;
		try {
			loading.play();
			rowMap[`${secId}${rowId}`].props.closeRow();
			let response = await DeleteTodo({ variables: { id } });
			await QueryTodo.refetch();
			loading.stop();
			showToast(_.get(response, 'data.deleteTodo', ''));
		} catch (e) {
			loading.stop();
			showToast(_.get(e, 'message', 'Something went wrong'), true);
		}
	};

	render() {
		let { todo, spinner, isDialogVisible, editedTodo } = this.state;
		let { DeleteTodo, QueryTodo } = this.props;

		return (
			<Container>
				<Header />
				<CusInput
					onPress={this.handleCreateTodo}
					value={todo}
					onChangeText={(text) => {
						this.setState({
							todo: text
						});
					}}
				/>

				{!QueryTodo.loading ? (
					<TodoList
						onDelete={this.handleDeleteTodo}
						onEdit={this.handleEdit}
						todos={_.get(QueryTodo, 'todos', [])}
					/>
				) : QueryTodo.error ? (
					<Text>Something went wrong</Text>
				) : (
					<ActivityIndicator size="small" />
				)}

				<Dialog.Container visible={isDialogVisible}>
					<Dialog.Title>Edit Todo</Dialog.Title>
					<Dialog.Input value={editedTodo} onChangeText={this.handleChangeEditedTodo} />
					<Dialog.Button label="Cancel" onPress={this.handleCloseDialog} />
					<Dialog.Button label="Edit" onPress={this.handleEditTodo} />
				</Dialog.Container>
			</Container>
		);
	}
}

export default TodoApp;
