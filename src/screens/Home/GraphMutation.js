import gql from 'graphql-tag';

const MUTATION = gql`
	mutation CreateTodo($todo: String!, $isSelect: Boolean) {
		createTodo(todo: $todo, isSelect: $isSelect) {
			id
			todo
			isSelect
		}
	}
`;

const DELETE_MUTATION = gql`
	mutation DeleteTodo($id: String!) {
		deleteTodo(id: $id)
	}
`;

const UPDATE_MUTATION = gql`
	mutation UpdateTodo($id: String!, $todo: String!) {
		updateTodo(id: $id, todo: $todo) {
			todo
			id
			isSelect
		}
	}
`;

export { MUTATION, DELETE_MUTATION, UPDATE_MUTATION };
export default MUTATION;
