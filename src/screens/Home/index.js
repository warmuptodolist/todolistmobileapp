import TodoApp from './HomeView/TodoApp';
import { Query, Mutation, graphql, compose } from 'react-apollo';
import QUERY from './GraphQuery';
import { MUTATION, DELETE_MUTATION, UPDATE_MUTATION } from './GraphMutation';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
	loading: state.appReducer.loading
});

export default compose(
	graphql(MUTATION, {
		name: 'CreateTodo',
		options: {
			update: (cache, { data: { createTodo } }) => {
				const { todos } = cache.readQuery({ query: QUERY });
				console.log('this is my todos', todos, createTodo);
				cache.writeQuery({
					query: QUERY,
					data: { todos: todos.concat([ createTodo ]) }
				});
			}
		}
	}),
	graphql(DELETE_MUTATION, { name: 'DeleteTodo' }),
	graphql(QUERY, { name: 'QueryTodo' }),
	graphql(UPDATE_MUTATION, { name: 'UpdateTodo' }),
	connect(mapStateToProps)
)(TodoApp);
